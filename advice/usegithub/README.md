---
Title: Use GitHub for Most Things, Not GitLab
Subtitle: How GitHub has Surpassed Gitlab
---

2020 has been an amazing year for GitHub. People have really wondered
what would become of GitHub given all the controversy when Microsoft
purchased it some years back, but whatever the reason GitHub *dominated*
by the beginning of 2021. Above all I believe this is because GitHub
focuses --- above all --- on making developers' lives easier and more
fun.

* Microsoft backed
  * Not your dad's (evil) Microsoft
  * Values "developers, developers, developers"
  * Knows when to share, and when to charge
  * Can't be run from home, but who cares
  * Doesn't give away the farm
  * More capital security, no begging
  * Doesn't need to brag about "valuation"
  * Enjoys Silicon Valley advantages
  * Not "hyper transparent"
  * Longevity and stability
  * Doesn't depend on open source contributions, but benefits
* GitHub focused *primarily* on code management, not "complete devops"
* GitHub doesn't have role permissions, but who cares
* GitHub has [40 million
  users](https://expandedramblings.com/index.php/github-statistics/) (10
  million beginning of 2019)
* GitLab has [100,000
  users](https://expandedramblings.com/index.php/gitlab-statistics/)
  (80k beginning of 2019)
* GitHub responds to developer cultural values
  * Move from `master` to `main`
  * Skin color selection
  * Political involvement
  * DMCA take downs
  * ICE issue
* GitHub promotes open source more than GitLab
  * Sponsorships
  * Donation listings
  * Contributions from the company
  * Free private repos for open source orgs
  * Not open source, but releases open source libs
* GitHub has superior social media integration
  * Developer profile pages
  * Developer status
  * Social media cards FTW
  * Project icons not needed
  * Gist integrations
* GitHub has one of the best web APIs ever made
  * Comprehensive CLI tool (`gh`) in Go
  * Digital Ocean ("developer cloud") integration
* GitHub has far better marketing and promotion
  * More variations on "cute" Octokitty
  * Less enterprise-y
  * Better shwag
* GitHub has better foothold in education
  * Entire education division
  * Educational promotions
  * Curriculum integration
  * REPL.it integration
* GitHub doesn't need mirroring (`git` does it)
* GitHub project documentation support superior
  * Equivalent editor
  * Support for CommonMark
  * Better wikis
* GitHub UI remains superior to GitLab
  * Faster overall interaction
  * More intuitive and minimal
  * Configurations are counter-intuitive
  * Does not depend on JavaScript
  * Far better night mode
  * Flat structure constraint is good
* GitHub pages is easier to use than GitLab's
* GitHub now has equal or superior cloud offering
  * GitHub actions far superior to fixed GitLab CI
  * CI/CD is modular, not all or nothing
  * Not distracted by maintain in-house CI offering
  * Integrates with Azure cloud
* GitHub makes exporting difficult, to its advantage
* GitHub beat GitLab with their "marketplace"
* GitHub has a solid enterprise offering
  * More developers prefer GitHub (and will at work)
