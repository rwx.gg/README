---
Title: GitLab
Subtitle: World's *Best* Git Hosting Service
Query: true
---

GitLab is a [far superior](isbest/) alternative to [GitHub](../github/) mostly because of its independent and [open source](/what/open/source/) nature and [DevOps](/what/devops/) built in integration, but also because of its pure and elegant [UX](/what/hci/ux/).

