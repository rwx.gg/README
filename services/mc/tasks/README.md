---
Title: Minecraft Server Tasks
Query: true
---

* Set Up Minecraft Server
* [Start Minecraft Server](./start/)
* Stop Minecraft Server
* Enable Command Blocks
* Install a Plugin
