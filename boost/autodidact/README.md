---
Title: Autodidactic Habits of a Good Technologist
Subtitle: How to Get Good and Stay Good
---

* We are Born to Learn and Create

* What Exactly is an Autodidact?
  * Naturally internally motivated
  * More than an "independent" learner
  * Not the same as "self taught"
    * No "teachers", only learners and mentors
  * Examples: 
    * Da Vinci
    * Edison
    * TJ Holowaychuk
    * Most Any Scientist
    * Mr. Rob
      * Mr. Poulson
      * Mr. Bevans
      * Professor Lundquist 
      * Dr. Rogers
      * Doug the Hockey-Playing, Ophthalmologist
    * Doris

* Know Thyself: The Art of Personal Reflection
  * Going for walks and thinking
  * Try a lot of stuff
  * Capture your thoughts
  * Evaluate your preferences
  * Ask for feedback
  * Celebrate your wins ("brag sheet")

* Decide What You Enjoy
  * In terms of daily activities
  * Focus on what you *don't* enjoy (if needed)
  * Identify occupations that do those things
  * Should I learn tech?
  * You do you

* Managing Your Personal Skill Stack
  * Born from need to keep up
  * What | Current | Goal
  * Circle Priority Have to Do
  * Circle Most Want to Do

* Prescience with Purpose (PTP)
  * Seeing *and Catching* the Wave
  * Personal/Professional Learning Networks
  * Twitter / GitHub / YouTube / Twitch
    / Reddit / Linkedin / Newsgroups
  * Pros and Cons of "head down" (flow state)
    * FOMO
  * Story: Riding Out the DotCom Crash of 2000
  * Read Job Postings
  * Automated searches send to email
  * Study and Contact Companies
  * Employment is About Trust

* RWX Method of Learning
  * Contrast with Other Methods
    * "Banker" Pedagogy
    * Harry Potter, Order of the Phoenix (Dolores Umbridge)
    * Particle Fever
    * "Practical" is an academic insult
  * RWX Method
    * Reading / Reviewing / Researching / Recognizing
    * Writing / Wondering / Wishing
      * Deep Work
    * Exercising / Executing / Experimenting / Examining 
      / Explaining / Expanding
    * (repeat)

* Just-In-Time Learning
  * Named after JIT compilation
  * Don't let imposter syndrome overwhelm you
  * Never *fall* prey to Dunning-Krueger effect
  * *Note* the person from the cubical next to yours
  * Being inquisitive is different

* Preparing a Plan
  * How much time do I have?
    * Triathlon Training
    * Use It or Lose
  * Comprehensive: Courses, Books, Walkthroughs, Videos
    * Should You Certify?
  * Repetitive: Exercises, Recipes
  * Fun: Challenges, What-If, Competition, Gamified, Stuff you *Want* to
    Make, Projects
  * Change It When You Want

## See Also
  * [Deep Work](https://duck.com/lite?kd=-1&kp=-1&q=Deep Work)
  * [Make It Stick (Henry L. Roediger III)](https://duck.com/lite?kd=-1&kp=-1&q=Make It Stick (Henry L. Roediger III))
  * [Learning to Learn (Barbara Oakley)](https://duck.com/lite?kd=-1&kp=-1&q=Learning to Learn (Barbara Oakley))
  * [The Subtle Art of Not Giving a F*ck](https://duck.com/lite?kd=-1&kp=-1&q=The Subtle Art of Not Giving a F*ck)
  * [Seven Habits of Highly Effective People](https://duck.com/lite?kd=-1&kp=-1&q=Seven Habits of Highly Effective People)
