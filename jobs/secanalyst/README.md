---
Title: Security Analyst
Subtitle: A Hacker By Any Other Name
Query: true
---

The title *Security Analyst* is taken from the [BLS](https://www.bls.gov/ooh/computer-and-information-technology/information-security-analysts.htm) but we are really talking about hackers here, whether they be on the blue team (defense) or red team (offense) or no team (forensics). Hacking is currently the fastest growing technology profession by a long margin when [compared against others](/jobs/) but has a rather unique set of skills required that go far beyond the purely technical.
