---
Title: What is Computer Science?
Subtitle: The Science of Computers, Not Making Apps
Query: true
---

Computer science is literally the science of how computers work and can be used to compute stuff, which includes all sorts of different approaches (algorithms) and how to deal with data (data structures).

Although computer science covers how programming a computer works, it frequently does *not* include learning the *skill* of programming and developing applications itself. This is why so many receive CS degrees from college never actually get a lot of practice programming. 

Unfortunately, very few offer learning opportunities in the field of programming. This is why so many alternative forms of education exist for software engineering, all with varying degrees of relevance and value

Thankfully you can learn everything you need to know about software development simply by doing a lot of it on your own and studying existing open source code. You can even contribute to such projects providing proof you can demonstrate to others that you can do it for them as well.

## See Also

* [Crash Course Computer Science](https://www.youtube.com/playlist?list=PL8dPuuaLjXtNlUrzyH5r6jN9ulIgZBpdo)
