---
Title: What are Easter Eggs?
Subtitle: Dyed Aborted Chicken Embryos
Query: true
---

Dyed aborted chicken embryos allegedly delivered by a large pagan bunny to good Christian children celebrating the resurrection of their god. 

Okay maybe not *those* Easter eggs.

Also the things left in any creative work by one or more of the creators only to be found by skillful, inquisitive fans after a bit of exploration, research, and good guessing.

Easter eggs were popularized in the film, [Ready Player One](https://duck.com/lite?kae=t&q=Ready Player One).
