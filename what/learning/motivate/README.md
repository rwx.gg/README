---
Title: Early Motivators for Tech Learning
Subtitle: Keeping It Fun
Query: true
---

Having a specific thing in mind makes learning the boring stuff easier because you know what you are *eventually* going to get out of it. People are motivated by different things. Here are a few examples of project goals that tend to motivate early-stage technologists. Consider picking one and [writing](/what/rwx/w/) it down as part of your [learning plan](https://duck.com/lite?kae=t&q=learning plan).

1. Set Up a Minecraft Server
1. Develop a 2D Web Game
1. Create a Web Site
1. Hack and Defend (CTF Games)
1. Get a Job

