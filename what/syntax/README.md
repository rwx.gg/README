---
Title: What is Syntax?
Subtitle: The Rules of Expression
Query: true
---

A *syntax* is the grouping of rules and principles we use to express something in a specific way, be it through natural language or [computer languages](/lang/). Each language has a different syntax. When you write something that breaks the rules in a given coding language you get a "syntax error."

:::co-tip
Most syntax errors are caused by leaving stuff off of the end of a line or block, a semicolon, comma, curly bracket, etc. The best way to avoid these common errors is to add the closers immediately when adding the opener so you don't forget later while coding the content that goes inside of the thing.

Don't underestimate the power of clean style. Syntax errors creep in when the code becomes hard to read, say because you forgot to indent. Good style is *so* important some languages have designed it *into* the language syntax itself, such as Python's [significant whitespace](/what/whitespace/).
:::
