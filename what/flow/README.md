---
Title: What is Flow State?
Subtitle: The Secret to Getting Stuff Done
Query: true
---

A *flow state* is when you are "in the zone," full of energy and focus from being entirely taken over by the activity. One indicator you have been in flow state is when you completely lose track of time, either it seems like it passed more slowly or quickly.

Psychologist Mihály Csíkszentmihályi describes the mental state of flow:

> "[Flow is] being completely involved in an activity for its own sake. The ego falls away. Time flies. Every action, movement, and thought follows inevitably from the previous one, like playing jazz. Your whole being is involved, and you're using your skills to the utmost."

## See Also

* [Deep Work](https://www.amazon.com/Deep-Work-Focused-Success-Distracted/dp/1455586692)
