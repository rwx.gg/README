---
Title: What is Computer Networking?
Subtitle: Connecting Two Computerized Devices
Query: true
---

Understanding how your [home network](https://duck.com/lite?kae=t&q=home network) and the Internet work is an essential skill for any technologist today. Those who really enjoy it can work toward [Network Engineer occupations](https://duck.com/lite?q=Network Engineer occupations) with certifications in setting up and maintaining networks professionally.
