---
Title: What is Infrastructure as a Service / IaaS?
Subtitle: Providing Server Hosting from the Cloud
Query: true
---

*IaaS* is [infrastructure](/what/infra/) provided as a service allowing it to be turned up or down depending on varying needs at different times. Usually this is what people mean when they say "cloud" services (although that is [not all](../)). [Amazon (AWS)](), [Microsoft (Azure)](), [Google (Compute Engine)](), [Digital Ocean](), and [Linode]() are all examples of IaaS providers.

## See Also

* [Cloud Services](../)
