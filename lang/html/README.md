---
Title: Hyper Text Markup Language / HTML
Subtitle: The *Content* of the Web
Query: true
---

*HTML* is the main [language](/lang/) for the [World Wide Web](https://duck.com/lite?kae=t&q=World Wide Web). Every [web site](https://duck.com/lite?q=web site) has some HTML in it. HTML is one of the most important coding language anyone will learn because it is so [ubiquitous](/what/ubiquitous/). Every single technical occupation requires some knowledge of HTML.
