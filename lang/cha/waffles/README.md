---
Title: Waffles Program Challenge
Tags: moderate
---

Write a command-line program that simulates the ancient meme video [Do
You Like Waffles](https://youtu.be/UtlaTNI1TaU) prompting the user with
questions and checking their responses. If `yes` then ask the next
question. If `no` then print something snarky and end the program. If
nothing is entered (or just empty spaces) then ask the same question
again.

## Requirements


## Bonus

* Accept more than just `yes` as a positive response
* Accept more than just `no` as a negative response
* Add one more more functions
* Use nothing but short-circuit logic using functions
