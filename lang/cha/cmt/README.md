---
Title: Comment Command Challenge
Subtitle: Create a Comment Command
Tags: moderate
---

Create a program called `cmt` that reads in every line of input and adds
a comment prefix to each line and outputs the newly prefixed line. Once
that's complete have the program check for arguments and use them as the
prefix instead.

## Requirements

* Must include a *safe* shebang line if a script.
* Demonstrate how to use within Vi/m combined with `!`.
