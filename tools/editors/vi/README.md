---
Title: Vi Text Editor
Subtitle: The World's Most Ubiquitous Editor 
Query: true
---

>  "Vi was created in 1976 and opening it in a terminal is opening a wormhole to 1976" [\@type_theory](https://twitch.tv/type_theory)

Vi was created in 1976 and is the second oldest editor on the planet, third if you count `ex` upon which `vi` is built. It is the second editor to be added to the [UNIX POSIX standard](https://duck.com/lite?kae=t&q=UNIX POSIX standard), which has also made it the most [ubiquitous](/what/ubiquitous/). The first POSIX editor was [Ed](/tools/editors/ed/) created in 1969. The letters `vi` actually stand for `visual`, as in, a "visual" version of the [Ex editor](https://duck.com/lite?q=Ex editor). Anyone wanting a career involving Linux and UNIX systems must learn Vi even if they prefer something else for larger project development. 

The `vi` command is usually some form of [`vim`](/tools/editors/vim/) on most systems today, but not all. The closest thing to the original `vi` is a modern project called [`nvi`](http://www.bostic.com/vi/) which even recreates the bugs of the original. Comparing `nvi` to `vim` is one reliable way to know if what you are learning is a [vimism](/tools/editors/vim/vimisms/) or not.

## See Also

*Even though most of these are for Vim they are a pretty good way to learn Vi as well even though they include [Vimisms](/tools/editors/vim/vimisms/).*

* `vimtutor`
* <https://openvim.com>
* <https://vimgenius.com>
* <http://yannesposito.com/Scratch/en/blog/Learn-Vim-Progressively/>
* `:help` (from within Vim)

:::co-warning
Stay away from Vim Adventures. As fun as it is, it does not make a direct association to editing actual files that these other resources do.
:::

